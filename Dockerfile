FROM debian:latest

# Metadata
LABEL mantainer="Gius. Camerlingo <gcamerli@gmail.com>"
LABEL version="1.0"
LABEL description="CUPS server and client for local printers."

# Docker image name
ENV NAME=cups

# Timezone
ENV TZ="Europe/Paris"

# Update system and install cups server and client
RUN apt-get update
RUN apt-get install -y --no-install-recommends \
	curl \
	cups \
	cups-client \
	cups-pdf \
	whois \
	cron \
	gimp \
	wget \
	gcc \
	build-essential \
	printer-driver-gutenprint

# Clean system
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Create ssl dir
RUN mkdir -p /etc/cups/ssl

# Copy scripts inside the cups container
COPY config /etc/cups
COPY include /etc/pam.d/cups

# Disbale some cups backend that are unusable within a container
RUN mv /usr/lib/cups/backend/parallel /usr/lib/cups/backend-available/
RUN mv /usr/lib/cups/backend/serial /usr/lib/cups/backend-available/

# Healthcheck
COPY healthcheck /usr/local/bin/
RUN chmod 744 /usr/local/bin/healthcheck

# Crontab
COPY cron/crontab /etc/cron.d/scan-printers
RUN chmod 644 /etc/cron.d/scan-printers

# Scan script
COPY cron/scan-printers.sh /home/cups/scan-printers.sh
RUN chmod 744 /home/cups/scan-printers.sh

# User
RUN useradd -ms /bin/bash cups

# Home
ENV HOME=/home/cups

# Permissions
RUN chown -R cups:cups $HOME

# Set startup script
WORKDIR $HOME
COPY start_cups.sh $HOME/start_cups.sh
RUN chmod +x $HOME/start_cups.sh

# Change user
USER cups

# Set a command entrypoint
CMD ["/home/cups/start_cups.sh"]
