#!/bin/bash

scan=$(lpstat -v | awk '{ print $3 }' | cut -d : -f 1)

lpstat -v &> /dev/null

if [ $? != 0 ]; then
  exit 1
else
  echo $scan > /tmp/printers.log
  sort -u /tmp/printers.log | tr ' ' '\n'
fi
