# **Printer**

CUPS server and client for local printers.

### **Credits**

+ [ticosax/cups-in-docker](https://github.com/ticosax/cups-in-docker)

### **MIT License**

This work is licensed under the terms of **[MIT License](https://bitbucket.org/42spixel/printer/src/master/LICENSE.md)**.
